variable "yc_zone" {
  type    = string
  default = "ru-central1-a"
}

variable "vpc_network_name" {
  type    = string
  default = "net_230819_common"
}

variable "vms_subneta_name" {
  type    = string
  default = "vmsubnet_common"
}

variable "vms_subneta_zone" {
  type    = string
  default = "ru-central1-a"
}

variable "vms_subneta_v4_cidr_blocks" {
  type    = list(string)
  default = ["192.168.10.0/24"]
}

variable "ssh_key_user" {
  type    = string
  default = "ubuntu"
}

variable "ssh_key_path" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

variable "image_family" {
  type    = string
  default = "ubuntu-2204-lts"
}

variable "labels" {
  type = map(string)
  default = {
    "ansible_group" = "gl"
  }
}

variable "nat" {
  type    = bool
  default = false
}

variable "apps" {
  type = map(any)
  default = {
    app1 = {
      name = "app01",
      labels = {
        ansible_group = "app"
      },
      nat = true
    },
    db1 = {
      name = "db01"
      labels = {
        ansible_group = "db"
      },
      nat = true
    },
    vpn1 = {
      name = "vpn01"
      labels = {
        ansible_group = "vpn"
      },
      nat = true
    }
  }
}
