terraform {
  backend "s3" {
    key                         = "prod/vpc/terraform.tfstate"
    bucket                      = "do230819-tfstate-bucket"
    endpoint                    = "storage.yandexcloud.net"
    region                      = "ru-central1"
    skip_region_validation      = true
    skip_credentials_validation = true

    # AWS_ACCESS_KEY_ID
    # access_key = ""
    # AWS_SECRET_ACCESS_KEY
    # secret_key = ""
  }

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">=0.82.0"
    }
  }
}

provider "yandex" {
  # YC_TOKEN
  # token = 
  # YC_CLOUD_ID
  # cloud_id = 
  # YC_FOLDER_ID
  # folder_id = 
  zone = var.yc_zone
}

data "yandex_resourcemanager_cloud" "cloud" {
  name = "do230819"
}

data "yandex_resourcemanager_folder" "prod_folder" {
  cloud_id = data.yandex_resourcemanager_cloud.cloud.id
  name     = "prod"
}

resource "yandex_vpc_network" "vpc_network" {
  folder_id = data.yandex_resourcemanager_folder.prod_folder.id
  name      = var.vpc_network_name
}

resource "yandex_vpc_subnet" "vms_subneta" {
  folder_id      = data.yandex_resourcemanager_folder.prod_folder.id
  name           = var.vms_subneta_name
  zone           = var.vms_subneta_zone
  network_id     = yandex_vpc_network.vpc_network.id
  v4_cidr_blocks = var.vms_subneta_v4_cidr_blocks
}

data "yandex_compute_image" "image" {
  family = var.image_family
}

module "vpc" {
  for_each     = var.apps
  source       = "git::https://gitlab.com/klovtsov/devops_includes/terraform_modules/yc-vpc.git?ref=main"
  name         = each.value.name
  hostname     = each.value.name
  labels       = each.value.labels
  nat          = each.value.nat
  ssh_key_path = var.ssh_key_path
  folder_id    = data.yandex_resourcemanager_folder.prod_folder.id
  subnet_id    = yandex_vpc_subnet.vms_subneta.id
  image_id     = data.yandex_compute_image.image.id
}
